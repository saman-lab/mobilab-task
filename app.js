const axios = require('axios');
const cheerio = require('cheerio');
const stream = require('stream');
const got = require('got');
const { promisify } = require('util');
const fs = require('fs');

const url = "https://www1.nyc.gov/site/tlc/about/tlc-trip-record-data.page";
const yearCount = 3;

const pipeline = promisify(stream.pipeline);

fetchData(url).then(async (res) => {
    const html = res.data;
    const $ = cheerio.load(html);
    const data = $('.faq-v1 > div').filter(function () {
        return $(this).attr('class') == "faq-answers";
    }).slice(0, yearCount);
    var _year = ""
    for await (const child1 of data) {
        _year = $(child1).attr('id').replace("faq", "");
        var _month = "";
        for await (const child2 of $(child1).children().children().children().children().children()) {
            if ($(child2).prop("tagName") != "UL") {
                _month = $(child2).children().text();
            } else {
                for await (const child3 of $(child2).children()) {
                    await pipeline(
                        got.stream($(child3).children().attr("href")),
                        fs.createWriteStream(_year + "-" + monthInt(_month) + "-" + $(child3).children().attr("title") + '.csv')
                    ).then(() => {
                        console.log(_year + "-" + monthInt(_month) + "-" + $(child3).children().attr("title"));
                    });
                }
            }
        }
    }
})

async function fetchData(url) {
    console.log("Crawling data...")
    let response = await axios(url).catch((err) => console.log(err));

    if (response.status !== 200) {
        console.log("Error occurred while fetching data");
        return;
    }
    return response;
}

function monthInt(month) {
    const monthDic = {
        "January": 1,
        "February": 2,
        "March": 3,
        "April": 4,
        "May": 5,
        "June": 6,
        "July": 7,
        "August": 8,
        "September": 9,
        "October": 10,
        "November": 11,
        "December": 12
    };
    return monthDic[month].toString().padStart(2, '0');
}